Deploy e Configuração
=====================

A estratégia usada na montagem implica em instâncias que já sobem configuradas, sendo assim esta se configura utilizando os playbooks do ansible. 
Todos os playbooks utilizados aqui também estão armazenados num repositório git.
    https://lidios@bitbucket.org/lidios/company_news_playbooks.git

Na inicialização de uma instância, ela executa o "pull" dos playbooks os executa localmente. 
Essa abordagem foi utilizada no setup para facilitar a utilização do autoscale, onde as instâncias são criadas dinamicamente e automaticamente.


Como instalar o Ansible
=======================
Utilize o arquivo de requirements.txt presenta na pasta para instalar o ansible e suas dependencias utilizando o pip.
    pip install -r requirements.txt 

Caso não tenha o pip instale utilizando easy_install ou seu gerenciador de pacotes.


Arquivos
=========
Os servidores de training e production devem ser listados no arquivo "hosts" para o caso de configuração remota.

Os artefatos a serem deployados estão configurados no arquivo vars.yml, juntamente com todas as variáveis utilizadas pelos playbooks.

Exemplo:
    war_file: https://s3.amazonaws.com/infra-assessment/companyNews.war
    static_files: https://s3.amazonaws.com/infra-assessment/static.zip


Execução local - Configuração das instâncias
==============

Cada instância executa os seguintes comando na inicialização para executar os playbooks.

    echo "localhost" > ~/ansible_hosts
    export ANSIBLE_HOSTS=~/ansible_hosts
    ansible-pull -U https://lidios@bitbucket.org/lidios/company_news_playbooks.git -d ~/playbooks

Sendo assim , caso queira-se alterar qualquer configuração usando o ansible de dentro da instância basta logar na máquina e executar esses comandos.
O ansible-pull utiliza o playbook local.yml para executar as tasks.


Execução remota
==============
Para deployar uma nova versão do sistema ou configurar um nó remotamente execute o seguinte comando. 

   ansible-playbook remote.yml --private-key=<key.pem> -i hosts 

É importante atualizar o arquivo hosts com o IP da(s) máquinas que se deseja configurar.


